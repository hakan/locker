# locker
extra simple tool for creating an encrypted secret safe.

## usage
```
~ $ ./lock.sh
Enter passcode:
~/dat $ # create and edit stuff here
~/dat $ exit
~ $ # your files are encrypted now
```

## dependencies
`ccrypt` and `tar` packages are required to run the script
