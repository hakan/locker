#!/bin/bash

if [ ! -f dat.tar.cpt ];
then
	mkdir dat
	tar cf dat.tar --remove-files dat/
	ccencrypt dat.tar
	if [ $? != 0 ]; then rm dat.tar; exit; fi
	echo "Secure archive created!"
fi

pass=""
printf "Enter passcode: "
stty -echo
read pass
stty echo
echo

cp dat.tar.cpt backup

echo "$pass" > pass
ccdecrypt -k pass dat.tar.cpt

if [ $? != 0 ]; then rm pass; exit; fi

tar xf dat.tar
rm dat.tar
cd dat
bash
cd ..
tar cf dat.tar --remove-files dat/

ccencrypt -k pass dat.tar

rm pass
